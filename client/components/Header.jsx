Header = React.createClass({
    render() {
        return (
            <h1  style={this.css.header}>Is Pincho Man At The Spot?</h1>
        )
    },
    
    css: {
        header: {
            fontSize: "60px",
            color: "black",
            width: "80%",
            margin: "0 auto",
            paddingTop: "20vh",
            textAlign: "center",
            fontWeight: "800",
            lineHeight: "1.4",
            letterSpacing: "2.1"
        }
    }
})