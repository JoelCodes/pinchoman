var text = " "

Sign = React.createClass({
    
    getInitialState() {
      return {
          open: false
      }  
    },
    
    render() {
        

        return (
            <div>
                <h1 id="sign" className="animated" style={this.getStatus()}>{text}</h1>
                <h6>{Date()}</h6>
                    <small onMouseOver={this.refresh}>refresh</small>
            </div>
                  
        )
    },
    
    getStatus() {
            
            var $s = $("#sign");
            
            
            if(this.state.open === false) {
                text = "Closed"
                return {
                    color: "red",
                    textAlign: "center",
                    width: "40%",
                    margin: "0 auto",
                    paddingTop: "2vh"
                }
                
            } else {
                       text = "Open"
                
                return {
                    color: "green",
                    textAlign: "center",
                    width: "40%",
                    margin: "0 auto",
                    paddingTop: "2vh"
                }
            }
            
            
    },
    
    
    refresh() {
       
            if(Math.random() <= 0.5) {
                this.setState({open: false})
            } else {
                this.setState({open: true})
            }
        
    }
})